/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <simpleprint/simpleprint.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int CopyFile (char *src, char *dst)
{
	// Open source
	int source = open (src, O_RDONLY);

	// Check
	if (source < 0)
		return PrintError ("mv", src);

	// Open target
	int target = open (dst, O_WRONLY | O_TRUNC | O_CREAT);

	// Check
	if (source < 0)
	{
		close (source);
		return PrintError ("mv", dst);
	}

	// Copy
	for (;;)
	{
		// Read up to 512 bytes
		unsigned char copybuffer[512];
		int nread = read (source, copybuffer, 512);

		// Check for zero or error
		if (nread == 0)
			break;
		else if (nread < 0)
		{
			close (source);
			close (target);
			return PrintError ("mv", src);
		}

		// Write nread bytes
		int n = nread;
		int pos = 0;

		do
		{
			int diff = write (target, copybuffer + pos, n);

			// Check for errors
			if (diff < 0)
			{
				close (source);
				close (target);
				return PrintError ("mv", dst);
			}

			pos += diff;
			n -= diff;
		} while (pos != nread);
	}

	close (source);
	close (target);
	return 0;
}

int main (int argc, char **argv)
{
	// Usage
	if (argc != 3)
	{
		SimplePrint ("Usage: mv <source> <target>\n");
		return 1;
	}

	// Try the rename syscall first
	if (rename (argv[1], argv[2]) < 0)
	{
		// Cannot rename across volumes, copy and delete instead
		if (errno == EXDEV)
		{
			// Copy
			int ret = CopyFile (argv[1], argv[2]);

			// Return if copy failed, error message printed by CopyFile
			if (ret)
				return ret;

			// Try unlink otherwise
			if (unlink (argv[1]) < 0)
				return PrintError ("mv", argv[1]);

			return 0;
		}

		// Error in rename, print it
		return PrintError ("mv", argv[1]);
	}

	return 0;
}
