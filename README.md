# Readme #

This is a simple version of mv. It first tries to use the rename syscall.
If if fails, mv assumes that the rename failed because the files are on
different volumes and then copies the data first instead.